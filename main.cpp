#include <iostream>
#include <boost/any.hpp>


int main()
{
	boost::any a = "Hello boost::any!";
	std::cout << boost::any_cast<const char*>(a) << std::endl;
	return 0;
}
